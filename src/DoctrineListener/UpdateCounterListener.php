<?php

namespace App\DoctrineListener;

use App\Generator\IdGeneratorAllowedEntity;
use App\Repository\CounterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Events;

#[AsDoctrineListener(event: Events::postPersist, priority: 500, connection: 'default')]
class UpdateCounterListener
{

    public function __construct(
        private CounterRepository      $counterRepository,
        private EntityManagerInterface $entityManager,
    )
    {
    }


    public function postPersist(PostPersistEventArgs $args): void
    {
        $entity = $args->getObject();

        foreach (IdGeneratorAllowedEntity::COUNTER_VALUE_ARRAY as $entityName => $counterName) {
            if ($entity instanceof $entityName) {
                $counter = $this->counterRepository->findOneBy(['label' => $counterName]);
                $counter?->setValue($counter->getValue() + 1);
                $this->entityManager->persist($counter);
                $this->entityManager->flush();

                break;
            }
        }
    }
}