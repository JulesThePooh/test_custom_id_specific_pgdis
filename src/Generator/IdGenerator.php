<?php

namespace App\Generator;

use App\Entity\Category;
use App\Entity\Counter;
use App\Entity\Movie;
use App\Repository\CounterRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use function PHPUnit\Framework\throwException;

class IdGenerator extends AbstractIdGenerator
{



    public function __construct(
        private CounterRepository $counterRepository
    )
    {
    }

    public function generate(EntityManager $em, $entity)
    {

        foreach (IdGeneratorAllowedEntity::COUNTER_VALUE_ARRAY as $entityName => $counterName) {
            if ($entity instanceof $entityName) {
                $counter = $this->counterRepository->findOneBy(['label' => $counterName]);
                if ($counter === null) {
                    $counter = new Counter();
                    $counter->setLabel($counterName);
                    $counter->setValue(1);
                    $em->persist($counter);
                    $em->flush();
                }

                $idValue = str_pad($counter->getValue(), 4, "0", STR_PAD_LEFT);
                return "X" . $idValue;
            }
        }
        throw new \Exception('Your entity id generator is set to custom, but the entityName was not found in the process (see : App\Generator\IdGenerator)');
    }
}