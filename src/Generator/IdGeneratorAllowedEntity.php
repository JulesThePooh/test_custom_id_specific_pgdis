<?php

namespace App\Generator;

use App\Entity\Category;
use App\Entity\Movie;

class IdGeneratorAllowedEntity
{
    const COUNTER_VALUE_ARRAY = [
        Movie::class => 'movie',
        Category::class => 'category'
    ];
}