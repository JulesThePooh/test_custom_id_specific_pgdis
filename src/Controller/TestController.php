<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Movie;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{


    public function __construct(
        private EntityManagerInterface $entityManager,
    )
    {
    }

    #[Route('/test_movie', name: 'app_test_movie')]
    public function indexMovie(): Response
    {

        $movie = new Movie();
        $movie->setLabel('loul');
        $this->entityManager->persist($movie);
        $this->entityManager->flush();

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }

    #[Route('/test_category', name: 'app_test_category')]
    public function indexCategory(): Response
    {

        $category = new Category();
        $category->setLabel('testLabel');
        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
}
